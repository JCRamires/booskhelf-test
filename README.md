# Bookshelf

## How to run this project

- Clone this repo
- Enter `bookshelf` folder
- Run `yarn install` or `npm install`
- Run `yarn start` or `npm run start`
- Open `http://localhost:3000/` in your browser