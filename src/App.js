import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Layout } from 'antd'

import Home from './views/Home.jsx'
import Book from './views/Book.jsx'
import BookCRUD from './views/BookCRUD.jsx'
import Category from './views/Category.jsx'

import 'antd/dist/antd.css'

function App() {
  return (
    <Layout>
      <Layout.Header></Layout.Header>
      <Layout.Content
        style={{ padding: '50px 50px', backgroundColor: 'white' }}
      >
        <Router>
          <Switch>
            <Route path="/book/add">
              <BookCRUD />
            </Route>
            <Route path="/book/:bookId/edit">
              <BookCRUD editing />
            </Route>
            <Route path="/book/:bookId">
              <Book />
            </Route>
            <Route exact path="/category">
              <Category />
            </Route>
            <Route path="/category/:category">
              <Category />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </Layout.Content>
    </Layout>
  )
}

export default App
