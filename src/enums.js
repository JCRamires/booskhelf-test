export const categories = {
    reading: 'Currently Reading',
    wantToRead: 'Want to Read',
    read: 'Read'
}