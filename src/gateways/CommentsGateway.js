import db from './database'

export function createComment(bookId, comment, author) {
  return db.comments.add({
    bookId,
    comment,
    author,
    timestamp: Date.now(),
    deleted: false,
  })
}

export function getCommentById(id) {
  return db.comments.where('id').equals(id).first()
}

export function getCommentsByBookId(bookId) {
  return db.comments.where('bookId').equals(bookId).toArray()
}

export function updateComment(commentId, data) {
  return db.comments.update(commentId, data)
}

export function deleteComment(commentId) {
  return updateComment(commentId, { deleted: true })
}
