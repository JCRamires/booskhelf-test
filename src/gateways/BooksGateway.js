import isNil from 'lodash/isNil'

import db from './database'

function checkValues(title) {
  if (isNil(title)) throw new Error('Title is required')
}

export function createBook(title, description, author, category) {
  try {
    checkValues(title, category)

    db.books.add({
      title,
      description,
      author,
      category,
      timestamp: Date.now(),
    })
  } catch (error) {
    throw error
  }
}

export function getAllBooks() {
  return db.books.toArray()
}

export function getBookById(id) {
  if (isNil(id)) throw new Error('id is required')

  return db.books.where('id').equals(id).first()
}

export function getBooksByCategory(category = '') {
  return db.books.where('category').equals(category).toArray()
}

export function updateBook(bookId, data) {
  return db.books.update(bookId, data)
}

export function deleteBook(bookId) {
  return updateBook(bookId, { deleted: true })
}