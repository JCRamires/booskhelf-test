import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Typography, PageHeader, Divider } from 'antd'

import { getBooksByCategory } from '../gateways/BooksGateway'

import { categories } from '../enums'
import { getBookList } from './utils'

import BookList from '../components/BookList.jsx'
import SelectOrderBy from '../components/SelectOrderBy.jsx'

function Category() {
  const [books, setBooks] = useState([])
  const [loading, setLoading] = useState(true)
  const [orderBy, setOrderBy] = useState('title')

  const { category } = useParams()

  useEffect(() => {
    async function getCategoryBooks() {
      try {
        const books = await getBooksByCategory(category)
        setBooks(books)
      } catch (error) {
        console.log(error)
      }

      setLoading(false)
    }

    getCategoryBooks()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <PageHeader
        title={category !== undefined ? categories[category] : 'No category'}
        onBack={() => window.history.back()}
      />
      {loading ? (
        <Typography.Title>Loading...</Typography.Title>
      ) : (
        <>
          <span style={{ marginRight: '10px' }}>Order by</span>
          <SelectOrderBy
            value={orderBy}
            defaultOrder="title"
            onChangeOrder={(value) => setOrderBy(value)}
          />
          <Divider />
          <BookList books={getBookList(books, category, orderBy)} />
        </>
      )}
    </>
  )
}

export default Category
