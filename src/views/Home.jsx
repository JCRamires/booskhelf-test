import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { PageHeader, Divider, Button, Typography, Row, Col } from 'antd'

import { getAllBooks } from '../gateways/BooksGateway'
import { getBookList } from './utils'

import BookList from '../components/BookList.jsx'
import SelectOrderBy from '../components/SelectOrderBy.jsx'

import { categories } from '../enums'

/**
 * Component used for the Home view
 */
function Home() {
  const [books, setBooks] = useState([])
  const [orderBy, setOrderBy] = useState('title')
  const history = useHistory()

  useEffect(() => {
    async function getBooks() {
      try {
        setBooks(await getAllBooks())
      } catch (error) {
        console.log(error)
      }
    }

    getBooks()
  }, [])

  function getCategorySections() {
    return (
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col sm={24} md={6}>
          <Typography.Title level={4}>
            <Link to="/category">No category</Link>
          </Typography.Title>
          <BookList books={getBookList(books, undefined, orderBy)} />
        </Col>
        {Object.keys(categories).map((category) => (
          <Col key={`category-${category}`} sm={24} md={6}>
            <Typography.Title level={4}>
              <Link to={`/category/${category}`}>{categories[category]}</Link>
            </Typography.Title>
            <BookList books={getBookList(books, category, orderBy)} />
          </Col>
        ))}
      </Row>
    )
  }

  return (
    <>
      <PageHeader title="Home" backIcon={false} />
      <div style={{ display: 'flex', width: '100%', alignContent: 'center' }}>
        <div style={{ flexGrow: 1 }}>
          <span style={{ marginRight: '10px' }}>Order by</span>
          <SelectOrderBy
            value={orderBy}
            defaultOrder="title"
            onChangeOrder={(value) => setOrderBy(value)}
          />
        </div>
        <div style={{ alignSelf: 'center' }}>
          <Button type="primary" onClick={() => history.push('/book/add')}>
            Add a book
          </Button>
        </div>
      </div>
      <Divider />
      {getCategorySections()}
    </>
  )
}

export default Home
