import React, { useState, useEffect } from 'react'
import { useParams, Link, useHistory } from 'react-router-dom'
import {
  Typography,
  PageHeader,
  Descriptions,
  Divider,
  Button,
  Empty,
  Comment,
  Modal,
  Popconfirm,
  message
} from 'antd'
import { EditOutlined, DeleteOutlined } from '@ant-design/icons'
import format from 'date-fns/format'

import { getBookById, updateBook, deleteBook } from '../gateways/BooksGateway'
import {
  getCommentsByBookId,
  createComment,
  deleteComment,
  updateComment,
} from '../gateways/CommentsGateway'

import CommentForm from '../components/CommentForm.jsx'
import CategorySelect from '../components/CategorySelect'

const { Title } = Typography

function Book(props) {
  const [loading, setLoading] = useState(true)
  const [book, setBook] = useState()
  const [deletedBook, setDeletedBook] = useState(false)
  const [bookNotFound, setBookNotFound] = useState(false)
  const [comments, setComments] = useState([])
  const [editingComment, setEditingComment] = useState(false)
  const [commentId, setCommentId] = useState()

  const history = useHistory()
  const { bookId } = useParams()

  useEffect(() => {
    async function getBook() {
      try {
        const bookFromDatabase = await getBookById(parseInt(bookId))

        if (bookFromDatabase !== undefined) {
          if (bookFromDatabase.deleted) {
            setDeletedBook(true)
          }

          setBook(bookFromDatabase)
        } else {
          setBookNotFound(true)
        }
      } catch (error) {
        setBookNotFound(true)
      }

      setLoading(false)
    }

    getBook()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  async function getComments() {
    try {
      const comments = await getCommentsByBookId(parseInt(bookId))
      setComments(comments.filter((comment) => comment.deleted === false))
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getComments()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  async function onClickDeleteBook() {
    try {
      await deleteBook(parseInt(bookId))

      history.push('/')
    } catch (error) {
      console.log(error)
    }
  }

  async function onChangeBookCategory(category) {
    try {
      await updateBook(parseInt(bookId), { category })

      const bookClone = { ...book, category }
      setBook(bookClone)
      
      message.success('Category changed')
    } catch (error) {
      console.log(error)
    }
  }

  async function onClickEditComment(commentId) {
    setCommentId(commentId)
    setEditingComment(true)
  }

  async function onClickDeleteComment(commentId) {
    await deleteComment(commentId)
    await getComments()
    message.success('Comment deleted')
  }

  function closeModal() {
    setEditingComment(false)
    setCommentId(undefined)
  }

  async function onSubmit(data) {
    try {
      const { author, comment } = data

      if (editingComment) {
        await updateComment(commentId, { author, comment })
        closeModal()
        message.success('Comment edited')
      } else {
        await createComment(parseInt(bookId), comment, author)
        message.success('Comment posted')
      }

      await getComments()
    } catch (error) {
      console.log(error)
    }
  }

  const getCommentList = () =>
    comments.map((comment) => (
      <Comment
        key={`comment-${comment.id}`}
        author={comment.author}
        content={comment.comment}
        actions={[
          <Button
            style={{ marginRight: '10px' }}
            icon={<EditOutlined />}
            onClick={() => onClickEditComment(comment.id)}
          />,
          <Popconfirm
            title="Delete comment?"
            okText="Yes"
            okType="danger"
            onConfirm={() => onClickDeleteComment(comment.id)}
          >
            <Button type="danger" icon={<DeleteOutlined />} />
          </Popconfirm>,
        ]}
      />
    ))

  /**
   * In this function i check whether the book was not found, removed or display its content
   */
  function getPageContent() {
    if (deletedBook) {
      return (
        <>
          <Title>This book was removed</Title>
          <Link to="/">Back to home</Link>
        </>
      )
    } else if (bookNotFound) {
      return (
        <>
          <Title>Book not found!</Title>
          <Link to="/">Back to home</Link>
        </>
      )
    }

    return (
      <>
        <PageHeader
          title={book.title}
          onBack={() => window.history.back()}
          extra={[
            <Button
              key={'edit-button'}
              icon={<EditOutlined />}
              onClick={() => history.push(`/book/${bookId}/edit`)}
            >
              Edit
            </Button>,
            <Popconfirm
              key={'delete-button'}
              title="Delete book?"
              okText="Yes"
              okType="danger"
              onConfirm={() => onClickDeleteBook()}
            >
              <Button icon={<DeleteOutlined />} type="danger">
                Delete
              </Button>
            </Popconfirm>,
          ]}
        ></PageHeader>

        <Descriptions>
          <Descriptions.Item label="Author">{book.author}</Descriptions.Item>
          <Descriptions.Item label="Date added">
            {format(new Date(book.timestamp), 'dd/MM/yyyy')}
          </Descriptions.Item>
          <Descriptions.Item label="Category">
            <CategorySelect
              value={book.category}
              onChange={(category) => onChangeBookCategory(category)}
            />
          </Descriptions.Item>
        </Descriptions>
        <Descriptions>
          <Descriptions.Item label="Description">
            {book.description}
          </Descriptions.Item>
        </Descriptions>

        <Divider />

        <Title level={4}>Leave a comment</Title>

        <CommentForm onSubmitComment={(data) => onSubmit(data)} />

        <Divider />

        <Title level={4}>Comments</Title>

        {comments.length > 0 ? (
          getCommentList()
        ) : (
          <Empty description="No comments" />
        )}

        {editingComment && (
          <Modal
            title="Edit comment"
            visible
            onCancel={() => closeModal()}
            footer={null}
          >
            <CommentForm
              editing
              commentId={commentId}
              onSubmitComment={(data) => onSubmit(data)}
            />
          </Modal>
        )}
      </>
    )
  }

  return <>{loading ? <Title>Loading...</Title> : getPageContent()}</>
}

export default Book
