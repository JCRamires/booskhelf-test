/**
 * I created this function because the value being sorted can be either a number or a string
 * and if it is a string i need it to be lowercased
 */
function getSortingValue(value) {
  if (typeof value === 'number') return value
  return value.toLowerCase()
}

function sortFunction(orderBy, a, b) {
  if (getSortingValue(a[orderBy]) < getSortingValue(b[orderBy])) {
    return -1
  }
  if (getSortingValue(a[orderBy]) > getSortingValue(b[orderBy])) {
    return 1
  }
  return 0
}

export function getBookList(books, category = '', orderBy = 'title') {
  // had to clone this because the sort function is mutable
  const booksClone = [...books]

  // I do this so the sort function has access to the orderBy value
  const sortFunctionWithOrderBy = (a, b) => sortFunction(orderBy, a, b)

  return booksClone
    .filter((book) => !book.deleted && book.category === category)
    .sort(sortFunctionWithOrderBy)
    .map((book) => book)
}
