import React, { useState, useEffect } from 'react'
import { PageHeader, Form, Input, Button, Typography } from 'antd'
import { SaveOutlined } from '@ant-design/icons'
import { useParams, useHistory } from 'react-router-dom'

import { createBook, getBookById, updateBook } from '../gateways/BooksGateway'

import CategorySelect from '../components/CategorySelect.jsx'

const layout = {
  labelCol: { span: 3 },
  wrapperCol: { span: 15 },
}
const tailLayout = {
  wrapperCol: { offset: 3, span: 15 },
}

/**
 * Component used for creating, updating or deleting books
 */
function BookCRUD({ editing = false }) {
  const [loading, setLoading] = useState(true)
  const [defaultFormValues, setDefaultFormValues] = useState({})
  const [disableSavebutton, setDisableSaveButton] = useState(false)

  const { bookId } = useParams()
  const history = useHistory()

  async function getBook() {
    const { title, description, author, category } = await getBookById(
      parseInt(bookId)
    )
    setDefaultFormValues({
      title,
      description,
      author,
      category,
    })
    setLoading(false)
  }

  useEffect(() => {
    if (editing) {
      getBook()
    } else {
      setLoading(false)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  async function onSubmit(data) {
    try {
      setDisableSaveButton(true)

      const { title, description, author, category } = data

      if (editing) {
        await updateBook(parseInt(bookId), { title, description, author, category })
      } else {
        await createBook(title, description, author, category)
      }

      history.push('/')
    } catch (error) {
      console.log(error)
    }
  }

  // TODO: Timestamp, deleted

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo)
  }

  return (
    <>
      <PageHeader
        title={`${editing ? 'Edit' : 'Add'} book`}
        onBack={() => window.history.back()}
      />
      {loading ? (
        <Typography.Title>Loading...</Typography.Title>
      ) : (
        <Form
          {...layout}
          name="title"
          onFinish={onSubmit}
          onFinishFailed={onFinishFailed}
          initialValues={defaultFormValues}
        >
          <Form.Item
            label="Title"
            name="title"
            rules={[{ required: true, message: 'Title is required' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Description" name="description">
            <Input.TextArea autoSize={{ minRows: 2, maxRows: 6 }} />
          </Form.Item>
          <Form.Item label="Author" name="author">
            <Input />
          </Form.Item>
          <Form.Item label="Category" name="category">
            <CategorySelect />
          </Form.Item>
          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              icon={<SaveOutlined />}
              disabled={disableSavebutton}
            >
              {editing ? 'Update' : 'Save'}
            </Button>
          </Form.Item>
        </Form>
      )}
    </>
  )
}

export default BookCRUD
