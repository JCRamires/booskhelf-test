import React from 'react'
import { List } from 'antd'
import { Link } from 'react-router-dom'

const BookList = ({ books }) => {
  return (
    <List
      bordered
      dataSource={books}
      renderItem={(item) => (
        <List.Item>
          <List.Item.Meta
            title={<Link to={`/book/${item.id}`}>{item.title}</Link>}
            description={item.author}
          />
        </List.Item>
      )}
    />
  )
}

export default BookList
