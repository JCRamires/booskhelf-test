import React from 'react'
import { Select } from 'antd'

import { categories } from '../enums'

const CategorySelect = ({ value, onChange }) => (
  <Select value={value} onChange={(value) => onChange(value)} defaultValue="" style={{ width: 200 }}>
    <Select.Option value="">No category</Select.Option>
    {Object.keys(categories).map((category) => (
      <Select.Option key={`category-${category}`} value={category}>
        {categories[category]}
      </Select.Option>
    ))}
  </Select>
)

export default CategorySelect
