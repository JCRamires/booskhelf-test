import React, { useState, useEffect } from 'react'
import { Form, Input, Button, Typography } from 'antd'

import { getCommentById } from '../gateways/CommentsGateway'

const layout = {
  labelCol: { span: 3 },
  wrapperCol: { span: 15 },
}
const tailLayout = {
  wrapperCol: { offset: 3, span: 15 },
}

function CommentForm({ editing = false, commentId, onSubmitComment }) {
  const [disableSubmitButton, setDisableSubmitButton] = useState(false)
  const [loading, setLoading] = useState(true)
  const [defaultValues, setDefaultValues] = useState({})

  const [form] = Form.useForm()

  async function getComment() {
    const { author, comment } = await getCommentById(commentId)
    setDefaultValues({ author, comment })
    setLoading(false)
  }

  useEffect(() => {
    if (editing) {
      getComment()
    } else {
      setLoading(false)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  async function onFinish(data) {
    setDisableSubmitButton(true)
    await onSubmitComment(data)
    form.resetFields()
    setDisableSubmitButton(false)
  }

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo)
  }

  return loading ? (
    <Typography.Title>Loading...</Typography.Title>
  ) : (
    <Form
      {...layout}
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      initialValues={defaultValues}
    >
      <Form.Item
        label="Name"
        name="author"
        rules={[{ required: true, message: 'Name is required' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Comment"
        name="comment"
        rules={[{ required: true, message: 'Comment is required' }]}
      >
        <Input.TextArea autoSize={{ minRows: 3, maxRows: 6 }} />
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit" disabled={disableSubmitButton}>
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}

export default CommentForm
