import React from 'react'
import { Select } from 'antd'

function SelectOrderBy({ value, defaultValue, onChangeOrder }) {
  return (
    <Select
      defaultValue={defaultValue}
      onChange={(value) => onChangeOrder(value)}
      value={value}
      style={{ width: '120px' }}
    >
      <Select.Option value="title">Title</Select.Option>
      <Select.Option value="author">Author</Select.Option>
      <Select.Option value="timestamp">Date created</Select.Option>
    </Select>
  )
}

export default SelectOrderBy
